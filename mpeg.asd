(in-package :cl-user)

(defpackage :mpeg.system
  (:use :cl :asdf))

(in-package :mpeg.system)

(defsystem :mpeg
  :author "Denis Bueno"
  :licence "BSD"
  :components ((:file "packages")
               (:file "mp3-parser":depends-on ("packages"))))