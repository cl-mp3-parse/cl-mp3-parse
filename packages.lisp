(in-package :cl-user)

(defpackage :com.gigamonkeys.binary-data
  (:use :common-lisp :cl-user)
  (:nicknames "BINARY-DATA")
  (:export #:define-binary-class
           #:define-tagged-binary-class
           #:define-binary-type
           #:read-value
           #:write-value
           #:*in-progress-objects*
           #:parent-of-type
           #:current-binary-object
           #:+null+))

(defpackage :mpeg
  (:use :cl :binary-data)
  (:documentation "MPEG Parser: more specifically, MP3 frame parser.")
  (:export #:mpeg-load))
