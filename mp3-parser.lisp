;;;; $Id: mp3-parser.lisp,v 1.1.1.1 2005/03/19 23:19:06 dbueno Exp $
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; This software is provided without warranty of any kind, and is
;;;; released under the BSD license. I reserve the right to change
;;;; that, but only in the "more permissive license" direction, if
;;;; such is possible.
;;;;
;;;; Copyright (c) 2004, Denis Bueno
;;;; All rights reserved.
;;;;
;;;; Redistribution and use in source and binary forms, with or
;;;; without modification, are permitted provided that the following
;;;; conditions are met:
;;;;
;;;;   Redistributions of source code must retain the above copyright
;;;;   notice, this list of conditions and the following
;;;;   disclaimer.
;;;;
;;;;   Redistributions in binary form must reproduce the above
;;;;   copyright notice, this list of conditions and the following
;;;;   disclaimer in the documentation and/or other materials
;;;;   provided with the distribution.
;;;;
;;;;   The name of the Denis Bueno may not be used to endorse or
;;;;   promote products derived from this software without specific
;;;;   prior written permission.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
;;;; CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
;;;; INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
;;;; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
;;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
;;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
;;;; ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
;;;; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
;;;; OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;;;; POSSIBILITY OF SUCH DAMAGE.
;;;;
;;;; Notes: Originally a ported some code written by a fried of mine,
;;;; Curtis Jones. After I understood the problem, I changed the the
;;;; implementation of READ-MP3-FRAMES significantly.
;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; TODO
;;;;
;;;; [ ] There's something wrong with the way the average bitrate is
;;;; calculated. Fix that.
;;;;
;;;; [ ] Eventually I'd like to use Peter Seibel's CLOS generic binary type
;;;; system. Get that working.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :mpeg)

(defconstant mpeg-layer-1 #x03)
(defconstant mpeg-layer-2 #x02)
(defconstant mpeg-layer-3 #x01)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (unless (boundp 'mpeg-style)
    (defconstant mpeg-style
      (make-array
       (list 2 4)
       :element-type '(unsigned-byte 32)
       :initial-contents
       '((0 5 4 3)                          ; mpeg-2
         (0 2 1 0))                         ; mpeg-1
       )))

  (unless (boundp 'mpeg-frequency)
    (defconstant mpeg-frequency
      (make-array
       (list 4 2)
       :element-type '(unsigned-byte 32)
       :initial-contents
       '((22050 44100)
         (24000 48000)
         (16000 32000)
         (    0     0)))))

  (unless (boundp 'channel-mode)
    (defconstant channel-mode
      (make-array
       (list 2 2)
       :element-type 'symbol
       :initial-contents
       '((stereo       joint-stereo)
         (dual-channel single-channel)))))

  ;;
  ;; the numbers above each column represent the
  ;; mpeg type and layer, respectively. the numbers
  ;; to the right of each row represent the value
  ;; encoded in the mpeg file.
  ;;
  (unless (boundp 'mpeg-bitrate)
    (defconstant mpeg-bitrate
      (make-array
       (list 15 6)
       :element-type '(unsigned-byte 16)
       :initial-contents
       ;; 1,1   1,2    1,3    2,1    2,2    2,3
       '((  0     0      0      0      0      0)         ; 0000
         ( 32    32     32     32     32      8)         ; 0001
         ( 64    48     40     64     48     16)         ; 0010
         ( 96    56     48     96     56     24)         ; 0011
         (128    64     56    128     64     32)         ; 0100
         (160    80     64    160     80     64)         ; 0101
         (192    96     80    192     96     80)         ; 0110
         (224   112     96    224    112     56)         ; 0111
         (256   128    112    256    128     64)         ; 1000
         (288   160    128    288    160    128)         ; 1001
         (320   192    160    320    192    160)         ; 1010
         (352   223    192    352    224    112)         ; 1011
         (384   256    224    384    256    128)         ; 1100
         (416   320    256    416    320    256)         ; 1101
         (448   384    320    448    384    320))))      ; 1110
    ))

(defmacro df (num)
  `(coerce ,num 'double-float))

(defun frame-header-p (arr &optional (pos 0))
  "See if there is a frame header starting at POS in ARR. checks
  that all the frame sync bytes are set."
  (and (= #xff (aref arr pos))
       (= #xf (ldb (byte 4 4) (aref arr (1+ pos))))))

(define-condition invalid-frame-header ()
  ((text :initarg :text :reader text)))

(define-condition invalid-layer-code ()
  ((code :initarg :code :reader code)))

(defun read-mp3-frames (filename &optional (spos 0))
  "Read from FILENAME, an MP3 file, and return a list
  of (simple-array (unsigned-byte 8))'s, which contain the frame
  data for each frame in the MP3. The arrays returned include the
  header data for each frame. If SPOS is specified, SPOS bytes
  starting from the beginning of the file stream will be skipped
  before attempting to parse the frames of the MP3. This is
  primarily useful because this parser doesn't (yet) assume the
  presence of ID3 tags. SPOS can hence be used to skip over the
  ID3 tag(s) before attempting to parse."
  (with-open-file (stream filename :element-type '(unsigned-byte 8))
    (file-position stream spos)
    (read-mp3-frames-rec stream)))
(defun read-mp3-frames-rec (stream &optional (framenum 0)
                            (stream-pos 0) (frames '())
                            (rlow 0d0)  ; bitrate low
                            (rhi  0d0)  ; bitrate hi
                            (ravg 0d0)  ; bitrate avg

                            &aux
                            (header-size 4)
                            (head
                             (make-array
                              header-size
                              :element-type '(unsigned-byte 8)
                              :initial-element 0))

                            (bitrate-code 0)         ; 4 bit bitrate code
                            (layer-code 0)           ; 2 bit layer code
                            (id-code 0)              ; 1 bit mpeg id
                            (frequ-code 0)           ; 2 bit frequency code
                            (pad-code 0)             ; 1 bit padding code
                            (protect-code 0) ; 1 bit protection code

                            (frame-count 0)) ; total number of frames
  (declare (optimize ;(safety 3) (debug 3)
            speed)
           (type (or null (simple-array (unsigned-byte 8) (4))) head)
           (type (unsigned-byte 32) bitrate-code layer-code id-code
                 frequ-code pad-code protect-code frame-count
                 framenum stream-pos)
           (type double-float ravg rlow rhi))
  (block :top
    ;; we shouldn't read anything else on EOF
    (when (zerop (setq header-size (read-sequence head stream)))
      (return-from :top (values (nreverse frames) framenum rlow rhi ravg)))
    (incf stream-pos header-size)

    (multiple-value-setq (head bitrate-code layer-code id-code frequ-code
                               pad-code protect-code)
      (read-mp3-header head))

    ;; calculate the values we're interested in: the actual bitrate of the
    ;; frame, its actual frequency, and its size (framebytes)
    (let* ((bitrate (bitrate-code->value bitrate-code id-code layer-code))
           (frequ (frequ-code->value frequ-code id-code))
           (framebytes (calculate-framebytes bitrate frequ pad-code layer-code))
           (data nil)
           (num-read 0))
      (declare (type (unsigned-byte 32) bitrate frequ framebytes num-read)
               (type (or null (simple-array (unsigned-byte 8) (*))) data))

      ;; it can sometimes happen that the calculated value is less than 0, in
      ;; cases where an image embedded in the ID3 tag of an mp3 is mistaken
      ;; for a frame header. so, we check for that. we don't explicitly try
      ;; to parse ID3 tags (that's what the SPOS) argument is for, but, what
      ;; the hey.
      (when (<= framebytes 0)
        (error "Calculated invalid length for framebytes (~a)."
               framebytes))

      ;; make an array of the appropriate size
      (setq data (make-array framebytes
                             :element-type '(unsigned-byte 8)
                             :initial-element 0))

      ;; copy the header into the data for the frame
      (replace data head)

      ;; record how much we read from the stream
      (setq num-read (read-sequence data stream :start header-size))

      ;; read the next frame with a recursive invocation, recording
      ;; necessary info about this frame
      (read-mp3-frames-rec stream
                           ;; number of frames we've seen
                           (1+ framenum)
                           ;; position in the stream
                           (+ num-read stream-pos)
                           ;; the frames themselves
                           (cons data frames)
                           ;; new lowest bitrate value
                           (if (or (zerop rlow) (> rlow bitrate))
                               (df bitrate) rlow)
                           (if (> bitrate rhi) (df bitrate) rhi)
                           ;; update a running average of the bitrate over
                           ;; all the frames
                           (/ (+ (* ravg frame-count) bitrate)
                              (1+ frame-count))))))

(defun read-mp3-header (data &optional (bufpos 0)
                         &aux
                         (head
                          (make-array 4
                                      :element-type '(unsigned-byte 8)
                                      :initial-element 0)))
  "Read the MP3 header from the given data buffer. The buffer
  should contain at least 4 bytes of data (that's how long
  headers are)."
  (unless (frame-header-p data bufpos)
    (error 'invalid-frame-header :data data))

  ;; read the 4 bytes that make up the header
  (replace head data :start2 bufpos :end2 (+ bufpos 4))

  ;; grab the coded values from the header...
  (let ((bitrate (ldb (byte 4 4) (aref head 2)))
        (layer   (ldb (byte 2 1) (aref head 1)))
        (mpegid  (ldb (byte 1 3) (aref head 1)))
        (frequ   (ldb (byte 2 2) (aref head 2)))
        (padding (ldb (byte 1 1) (aref head 2)))
        (protect (ldb (byte 1 0) (aref head 1)))
        (chan    (ldb (byte 2 6) (aref head 2))))
    ;; ... and return them
    (values head bitrate layer mpegid frequ padding protect chan)))

(defun calculate-framebytes (bitrate frequ pad-code layer-code)
  "Calculate the number of bytes in a given frame."
  (case layer-code
    ((or #.mpeg-layer-2 #.mpeg-layer-3)
     (floor
      (+ (/ (* bitrate 1000 144) frequ)
         (if (zerop pad-code) 0 1))))
    (#.mpeg-layer-1
     (floor
      (* (+ (/ (* bitrate 1000 12) frequ)
            (if (zerop pad-code) 0 4))
         4)))
    (t (error 'invalid-layer-code :code layer-code))))

(defun print-frame-header (head &optional (framenum nil))
  "Print the values in the frame header so that they make sense
  to a human. FRAMENUM is used if you want to label the frame
  you're printing."
  (assert (= 4 (length head)))
  (multiple-value-bind (h bitrate layer mpegid frequ padding protect chan)
      (read-mp3-header head)
    (declare (ignore h))
    (format t "~&MP3~:[~; Frame ~d~] Header Information~%" framenum framenum)
    (format t "~&  bitrate: ~,3f~%" (bitrate-code->value bitrate mpegid layer))
    (format t "~&  length:  0x~x bytes~%"
            (calculate-framebytes (bitrate-code->value bitrate mpegid layer)
                                  (frequ-code->value frequ mpegid)
                                  padding layer))
    (format t "~&  layer:   ~d~%" (layer-code->value   layer))
    (format t "~&  mpegid:  ~a~%" (id-code->value      mpegid))
    (format t "~&  frequ:   ~d Hz~%" (frequ-code->value frequ mpegid))
    (format t "~&  padding: ~d~%" (if (zerop padding) 'not-padded 'padded))
    (format t "~&  protect: ~d~%" (if (zerop protect) 'protected
                                      'not-protected))
    (format t "~&  channel: ~d~%" (channel-code->value chan))))

(defun bitrate-code->value (bitrate-code mpegid-code layer-code)
  "Map a bitrate code to its value."
  (aref mpeg-bitrate bitrate-code
        (aref mpeg-style mpegid-code layer-code)))

(defun layer-code->value (layer-code)
  "Map a layer code to its value."
  (case layer-code
    ;; I found a necessary use for #. notation: to evaluate a constant at
    ;; read-time!
    (#.mpeg-layer-1 'mpeg-layer-1)
    (#.mpeg-layer-2 'mpeg-layer-2)
    (#.mpeg-layer-3 'mpeg-layer-3)
    (t 'unknown)))

(defun id-code->value (id-code)
  "Map the mpeg id code to its value."
  (case id-code
    (0 'mpeg-2.5-unofficial)
    (1 'reserved)
    (2 'mpeg-2-iso/iec-13818-3)
    (3 'mpeg-1-iso/iec-11172-3)))

(defun frequ-code->value (frequ-code id-code)
  "Map the frequency code to its value."
  (aref mpeg-frequency frequ-code id-code))

(defun channel-code->value (channel-code)
  "Map the channel code to its value."
  (case channel-code
    (0 'stereo)
    (1 'joint-stereo)
    (2 'dual-channel)
    (2 'single-channel)))


;;;; EOF